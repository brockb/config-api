import graphene
import json

from flask import Flask, request
from flask_graphql import GraphQLView
from models import Ini, IniList, Codefile, CodefileList, AllList
from parser import IniParser, CodefileParser
from typing import Dict

app = Flask(__name__)

class Query(graphene.ObjectType):
  get_ini = graphene.Field(
    Ini,
    filename=graphene.String(),
    account=graphene.String()
  )

  get_inis = graphene.Field(
    IniList,
    filenames=graphene.List(graphene.String),
    account=graphene.String()
  )

  get_codefile = graphene.Field(
    Codefile,
    filename=graphene.String(),
    account=graphene.String()
  )
  
  get_codefiles = graphene.Field(
    CodefileList,
    filenames=graphene.List(graphene.String),
    account=graphene.String()
  )

  get_all = graphene.Field(
    AllList,
    codefiles=graphene.List(graphene.String),
    inifiles=graphene.List(graphene.String),
    account=graphene.String()
  )

  def resolve_get_ini(self, info, **args: Dict) -> Dict:
    filename = f'/home/web/csc/{args.get("account")}/data/config/reports/{args.get("filename")}'
    file = IniParser(filename)
    response = file.parse()
    response.update({"name": args.get("filename")})
    return response

  def resolve_get_inis(self, info, **args: Dict) -> Dict:
    response = []
    for file in args.get("filenames"):
      filename = f'/home/web/csc/{args.get("account")}/data/config/reports/{file}'
      inifile = IniParser(filename)
      filedata = inifile.parse()
      filedata.update({"name": file})
      response.append(filedata)

    return {"files": response}

  def resolve_get_codefile(self, info, **args: Dict) -> Dict:
    filename = f'/home/web/csc/{args.get("account")}/data/codefile/{args.get("filename")}'
    file = CodefileParser(filename)
    file.read_file()
    return { "codes": file.get_all()}

  def resolve_get_codefiles(self, info, **args: Dict) -> Dict:
    response = []
    for file in args.get("filenames"):
      filename = f'/home/web/csc/{args.get("account")}/data/codefile/{file}'
      codefile = CodefileParser(filename)
      codefile.read_file()
      response.append({
        "codes": codefile.get_all(),
        "name": file
      })

    return {"files": response}

  def resolve_get_all(self, info, **args: Dict) -> Dict:
    codefiles = []
    for file in args.get("codefiles"):
      filename = f'/home/web/csc/{args.get("account")}/data/codefile/{file}'
      codefile = CodefileParser(filename)
      codefile.read_file()
      codefiles.append({
        "codes": codefile.get_all(),
        "name": file
      })

    inifiles = []
    for file in args.get("inifiles"):
      filename = f'/home/web/csc/{args.get("account")}/data/config/reports/{file}'
      inifile = IniParser(filename)
      filedata = inifile.parse()
      filedata.update({"name": file})
      inifiles.append(filedata)

    return {
      "codefiles": codefiles,
      "inifiles": inifiles
    }

schema = graphene.Schema(query=Query)

app.add_url_rule(
  '/graphql',
  view_func=GraphQLView.as_view(
    'graphql',
    schema=schema,
    graphiql=True
  )
)

app.run(host='0.0.0.0', port=5000, debug=True)