import unittest
import shutil
import os
import random
import string

from parser import CodefileParser, IniParser

def random_string():
  letters = string.ascii_lowercase
  return ''.join(random.choice(letters) for i in range(10))

class TestCodefileParser(unittest.TestCase):

  def setUp(self):
    self.test_dir = random_string()
    if not os.path.isdir(self.test_dir):
      os.mkdir(self.test_dir)
    else:
      raise Exception(f'Directory {self.test_dir} already exists.')

    with open(f'{self.test_dir}/empty.txt', 'w') as file:
      file.write('')
    
    self.empty = CodefileParser(f'{self.test_dir}/empty.txt')
    self.empty.read_file()
    
    with open(f'{self.test_dir}/short.txt', 'w', encoding='ISO-8859-1') as file:
      file.write(f'{chr(254) * 10}')
    
    self.short = CodefileParser(f'{self.test_dir}/short.txt')
    self.short.read_file()

    with open(f'{self.test_dir}/emptyCodes.txt', 'w', encoding='ISO-8859-1') as file:
      file.write(f'{chr(254) * 20}')
    
    self.empty_codes = CodefileParser(f'{self.test_dir}/emptyCodes.txt')
    self.empty_codes.read_file()
    
    with open(f'{self.test_dir}/valid.txt', 'w', encoding='ISO-8859-1') as file:
      file.write(f'{chr(254) * 11}t{chr(254)}test')

    self.valid = CodefileParser(f'{self.test_dir}/valid.txt')
    self.valid.read_file()

  def tearDown(self):
    shutil.rmtree(self.test_dir)

  def test_get_codes(self):
    self.assertEqual(self.empty.get_codes(), [])
    self.assertEqual(self.short.get_codes(), [])
    self.assertEqual(self.empty_codes.get_codes(), [])
    self.assertEqual(self.valid.get_codes(), ["t"])

  def test_get_descriptions(self):
    self.assertEqual(self.empty.get_descriptions(), [])
    self.assertEqual(self.short.get_descriptions(), [])
    self.assertEqual(self.empty_codes.get_descriptions(), [])
    self.assertEqual(self.valid.get_descriptions(), ["test"])

  def test_get_all(self):
    self.assertEqual(self.empty.get_all(), [])
    self.assertEqual(self.short.get_all(), [])
    self.assertEqual(self.empty_codes.get_all(), [])
    self.assertEqual(self.valid.get_all(), [{"code": "t", "description": "test"}])

class TestIniParser(unittest.TestCase):

  def setUp(self):
    self.test_dir = random_string()
    if not os.path.isdir(self.test_dir):
      os.mkdir(self.test_dir)
    else:
      raise Exception(f'Directory {self.test_dir} already exists.')

    with open(f'{self.test_dir}/empty.txt', 'w') as file:
      file.write('')

    with open(f'{self.test_dir}/invalid.txt', 'w', encoding='ISO-8859-1') as file:
      file.write(f'{chr(254) * 10}')

    with open(f'{self.test_dir}/valid.txt', 'w', encoding='ISO-8859-1') as file:
      file.write(
      ''' 
        [header]
        test="test"
        testtwo = testtwo
        testthree=testthree
      '''
      )

    self.option = IniParser(f'{self.test_dir}/empty.txt')
    self.empty = IniParser(f'{self.test_dir}/empty.txt')
    self.invalid = IniParser(f'{self.test_dir}/invalid.txt')
    self.valid = IniParser(f'{self.test_dir}/valid.txt')

  def tearDown(self):
    shutil.rmtree(self.test_dir)

  def test_optionxform(self):
    self.assertEqual(self.option.optionxform('TEST'), 'TEST')
    self.assertEqual(self.option.optionxform('test'), 'test')
    self.assertEqual(self.option.optionxform('Test'), 'Test')

  def test_parse(self):
    self.assertEqual(self.empty.parse(), {})
    self.assertEqual(self.valid.parse(), {'test': 'test', 'testtwo': 'testtwo', 'testthree': 'testthree'})
    
    with self.assertRaises(UnicodeDecodeError):
      self.invalid.parse()

if __name__ == '__main__':
  unittest.main()