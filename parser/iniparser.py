from configparser import RawConfigParser
from typing import Dict

class IniParser(RawConfigParser):

  def __init__(self, file: str) -> None:
    self.file = file
    super().__init__(strict=False)

  def optionxform(self, string: str) -> str:
    return string

  def parse(self) -> Dict:
    response = {}

    with open(self.file, 'r') as file:
      self.read_string(f"[header]\n{file.read()}")

    for section in self.sections():
      for name in self.options(section):
        response.setdefault(name, self.get(section, name).strip('"'))
        
    return response