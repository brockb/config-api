from typing import List

class CodefileParser(object):
  def __init__(self, file: str) -> None:
    self.file = file

  def read_file(self) -> None:
    with open(self.file, encoding='ISO-8859-1') as file:
      self.data = file.read()

  def get_codes(self) -> List:
    codes = self.data.split(chr(254))
    if len(codes) > 11:
      codes = codes[11]
    else:
      return []

    return codes.split(chr(253)) if codes else []

  def get_descriptions(self) -> List:
    descriptions = self.data.split(chr(254))
    if len(descriptions) > 12:
      descriptions = descriptions[12]
    else:
      return []

    return descriptions.split(chr(253)) if descriptions else []

  def get_all(self) -> List:
    return [
      line = { "code": code, "description": description }
      for code, description in zip(self.get_codes(), self.get_descriptions())
    ]
