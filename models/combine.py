import graphene

from models.codefile import Codefile
from models.ini import Ini

class AllList(graphene.ObjectType):
  codefiles = graphene.List(Codefile)
  inifiles = graphene.List(Ini)