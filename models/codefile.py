import graphene

class CodeLine(graphene.ObjectType):
  code = graphene.String()
  description = graphene.String()

class Codefile(graphene.ObjectType):
  codes = graphene.List(CodeLine)
  name = graphene.String()

class CodefileList(graphene.ObjectType):
  files = graphene.List(Codefile)