import graphene

class Ini(graphene.ObjectType):
  name = graphene.String()
  ProgramName = graphene.String()
  Excel = graphene.String()
  WindowSize = graphene.String()
  WaitMinutes = graphene.String()
  WindowTitle = graphene.String()

class IniList(graphene.ObjectType):
  files = graphene.List(Ini)